'use strict'

$(function() {


/****************** Inicio del Programa ******************/

//Arreglo que contendrá los 256 números de forma ordenada
var papus_ordenados = [];

//Ponemos los números en el arreglo
for (let i = 0; i <= 255; i++)
    papus_ordenados.push(i);

//Generamos la tabla al principio para que se muestre
//ni bien carga la página
generarTabla();


/******************** Fin del Programa *******************/


/*
    ******************************
    * ACA DECLARAMOS LOS EVENTOS *
    ******************************
*/

//Creamos el evento click que generará una nueva tabla
$('#boton-para-refrescar').on('click', generarTabla);


/*
    ********************************
    * ACA DECLARAMOS LAS FUNCIONES *
    ********************************
*/

function generarTabla() {

    let abecedario = [];
    let papus = [];

    //Hacemos una copia de los papus ordenados con 'slice'
    papus = papus_ordenados.slice();

    //Desordenamos a los papus
    vamo_a_desordenarno(papus);

    //Ahora los papus estan en desorden
    //Que empieze el descontrol!!!

    //Movemos 2 papus a cada letra
    for (let i=0; i<27; i++) {

        let letra = [];

        letra.push(papus.pop());
        letra.push(papus.pop());

        abecedario.push(letra);
    }

    //Ya sacamos 54 papus, nos quedan 202, tenemos que
    //distribuirlos de manera aleatoria
    for (let i = 1; i <= 202; i++) {

        //Sacamos a un papu del grupo
        let  papu_alone = papus.pop();

        //Escogemos la letra a la irá papu alone
        //Validando que no pasen de 15 papus por letra
        let letra;

        do {
            let posicion_random = Math.floor(Math.random() * 27);
            letra = abecedario[posicion_random];
        }
        while(letra.length === 15);

        letra.push(papu_alone);
    }

    let tabla = $('#tabla-de-papus');

    //Limpiamos la tabla, porsiacaso
    tabla.empty();

    //Mostramos a todos los papus que están entre la A y la N
    for(let i = 0; i <= 13; i++)
        tabla.append('<li>' + String.fromCharCode(i + 65) + ' --> ' + abecedario[i] + '</li>');

    //Motramos a los papus de la letra Ñ
    tabla.append('<li>' + 'Ñ' + ' --> ' + abecedario[14] + '</li>');

    //Mostramos a los papus que sobran, de la O hasta la Z
    for(let i = 15; i<= 26; i++)
        tabla.append('<li>' + String.fromCharCode(i + 64) + ' --> ' + abecedario[i] + '</li>');

}

//Desordena a los papus de forma aleatoria
function vamo_a_desordenarno(arreglo) {
    let j, x, i;
    for (i = arreglo.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = arreglo[i - 1];
        arreglo[i - 1] = arreglo[j];
        arreglo[j] = x;
    }
}

});
